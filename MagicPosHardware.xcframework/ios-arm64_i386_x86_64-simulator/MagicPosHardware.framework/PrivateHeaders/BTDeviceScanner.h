//
//  BTDeviceScanner.h
//  Test
//
//  Created by xxxx on 19-11-8.
//  Copyright (c) 2019年 WL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@protocol BluetoothDelegateNormal<NSObject>

@optional
-(void)onBluetoothName:(NSString *)bluetoothName;
-(void)finishScanMPos;
-(void)bluetoothIsPowerOff;
-(void)bluetoothIsPowerOn;
@end



@interface BTDeviceScanner : NSObject

//bluetooth 
-(void)scanMPos: (NSInteger)timeout;
-(NSArray*)getAllOnlineMPosName;
-(void)stopMPos;
-(void)setBluetoothDelegate:(id<BluetoothDelegateNormal>)aDelegate;
-(CBCentralManagerState)getCBCentralManagerState;

@end





